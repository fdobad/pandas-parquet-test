# Recomendation
```
if self.args.debug||verbose:
	df.to_csv(file_name+'.csv')
	pd.read_csv(file_name+'.csv')
else:
	df.to_parquet(file_name+'.parquet', engine='fastparquet, compression=None)
	pd.read_parquet(file_name+'.parquet', engine='fastparquet')
```
Caveat column names must be string

# Tested
	- Debian Bullseye
	- Windows (no pyarrow nor orc)

# TODO
String data
